import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../security/security.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  error: string | null = null;
  newUserName = '';
  newPassword = '';
  newEmail = '';
  constructor(private security: SecurityService) { }

  ngOnInit() {
  }

  signup() {
    this.security.registerUser(this.newUserName, this.newPassword, this.newEmail).subscribe({
      complete: () => this.error = null,
      error: err => this.error = err,

    });
  }
}



