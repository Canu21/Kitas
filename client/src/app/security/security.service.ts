import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { share } from 'rxjs/operators';
import { newUser } from '../security/newuser';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  private user$ = new BehaviorSubject<User | null>(null);

  constructor(private http: HttpClient) {
    this.http.get<User | null>('/api/sessionUser').subscribe(
      user => this.user$.next(user),
    );
  }

  public getUser(): Observable<User | null> {
    return this.user$;
  }

  public login(userName: string, password: string): Observable<User> {

    const headers = new HttpHeaders({
      authorization: 'Basic ' + btoa(userName + ':' + password),
      'X-Requested-With': 'XMLHttpRequest',
    });

    const login$ = this.http.get<User>('/api/sessionUser', { headers }).pipe(share());

    login$.subscribe(
      user => this.user$.next(user),
      err => this.user$.next(null),
    );

    return login$;
  }

  registerUser(newusername: string, newpassword: string, newemail: string) {
    const body: newUser = {
      username: newusername,
      userEmail: newemail,
      password: newpassword,

    };
    return this.http.post('/api/signup', body);
  }


  logout() {
    const logout$ = this.http.post('/api/logout', {}).pipe(share());

    logout$.subscribe(() => this.user$.next(null));

    return logout$;
  }
}
