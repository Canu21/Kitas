import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../security.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  username = '';
  password = '';

  error: string|null = null;

  ngOnInit() {
  }

  constructor(private security: SecurityService, private http: HttpClient) {
  }

  login() {
    this.security.login(this.username, this.password).subscribe({
      complete: (() => {
        this.error = null;
      console.log('User: ' + this.username );
      }),
      error: err => this.error = err,
    });
  }

}
