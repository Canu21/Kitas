import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';



import { AppComponent } from './app.component';
import { LoginFormComponent } from './security/login-form/login-form.component';
import { UserInfoComponent } from './security/user-info/user-info.component';
import { StartseiteComponent } from './startseite/startseite.component';
import { SignupComponent } from './signup/signup.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { KitaComponent } from './kita/kita.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CalculatorComponent } from './calculator/calculator.component';








@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    UserInfoComponent,
    StartseiteComponent,
    SignupComponent,
    NavbarComponent,
    KitaComponent,
    CalculatorComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    MDBBootstrapModule,
    // AgmCoreModule.forRoot({apiKey: 'AIzaSyCnQwd-HsAmucdvjmeQnUWU8-M-CPHeD5s'}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
