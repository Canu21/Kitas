import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StartseiteComponent } from './startseite/startseite.component';
import { SignupComponent } from './signup/signup.component';
import { LoginFormComponent } from './security/login-form/login-form.component';
import { KitaComponent } from './kita/kita.component';
import {CalculatorComponent} from './calculator/calculator.component';



const routes: Routes = [
  { path: 'startseite', component: StartseiteComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginFormComponent },
  { path: 'kitas', component: KitaComponent },
  { path: 'calculator', component: CalculatorComponent},
  // { path: '**', redirectTo: StartseiteComponent },

];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],

})
export class AppRoutingModule { }
