export interface UserAdressDaten {
    strasse: string;
    hausnummer: string;
    postleitzahl: string;
    stadt: string;
}
