import { Component, OnInit } from '@angular/core';
import { UserAdressDaten } from './usermodel';
import { KitaService } from '../kita/kita.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent {

  calculator: UserAdressDaten = {
    strasse: '',
    hausnummer: '',
    postleitzahl: '',
    stadt: ''
  };


  constructor(private kitaservice: KitaService ) { }

  userEingabe() {
    this.kitaservice.getAllUserEingaben(this.calculator)
  }

}
