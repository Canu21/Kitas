import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Kita } from './kita';
import { HttpClient } from '@angular/common/http';
import { UserAdressDaten } from '../calculator/usermodel';


@Injectable({
  providedIn: 'root'
})
export class KitaService {


  private readonly apiUrl = '/api/kita';

  constructor(private http: HttpClient) { }


  getAllFreeKitas(): Observable<Kita[]> {
    return this.http.get<Kita[]>(this.apiUrl + '/freeKitas');
  }

  getAllKitas(): Observable<Kita[]> {
    return this.http.get<Kita[]>(this.apiUrl + '/getAll');
  }

  getAllUserEingaben(userAdressDaten: UserAdressDaten) {
    this.http.post<UserAdressDaten>(this.apiUrl + '/currentUserData', userAdressDaten).subscribe();
  }
}
