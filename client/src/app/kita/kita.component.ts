import { Component, OnInit } from '@angular/core';
import { Kita } from './kita';
import { KitaService } from './kita.service';

@Component({
  selector: 'app-kita',
  templateUrl: './kita.component.html',
  styleUrls: ['./kita.component.css']
})
export class KitaComponent implements OnInit {

  constructor(private kitaservice: KitaService) { }
  kitas: Kita[] = [];

  ngOnInit() {
   this.kitaservice.getAllKitas().subscribe(kitas => this.kitas = kitas);
  }

}
