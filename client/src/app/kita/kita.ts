export interface Kita {
    id: number;
    name: string;
    preis: number;
    adresse: {
        id: number;
        strasse: string;
        hausnummer: string;
        postleitzahl: string;
        wohnort: string;
    };
    gruppen: [
        {
            gruppeId: number;
            erzieher: string;
            kinderpfleger: string;
            anzahlMaxKinder: number;
            anzahlFreiePlaetze: number;
        }
    ];
    kitaLeitung: {
        id: number;
        vorName: string;
        nachName: string;
        profilText: string;
    };
}
