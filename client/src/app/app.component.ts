import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SecurityService } from './security/security.service';
import { User } from './security/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  constructor() {
  }

}

