package com.example.server;

import com.example.server.freiePlaetze.*;
import com.example.server.kita.Kita;
import com.example.server.kita.KitaRepository;
import com.example.server.security.WebSecurityConfiguration;
import com.example.server.user.User;
import com.example.server.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@Import(WebSecurityConfiguration.class)
public class ServerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

    @Autowired
    private KitaRepository kitaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AdresseRepository adresseRepository;

    @Autowired
    private BerufRepository berufRepository;

    @Autowired
    private GruppeRepository gruppeRepository;

    @Autowired
    private PersonRepository personRepository;

    @Override
    public void run(String... args) throws Exception {
        if (!userRepository.findByUsername("test").isPresent()) {
            userRepository.save(new User("test", passwordEncoder.encode("test123"), "test@hotmail.de"));
            userRepository.save(new User("caner", passwordEncoder.encode("123"), "caner.bingoel66@gmail.com"));
            userRepository.save(new User("christoph", passwordEncoder.encode("12345"), "christoph@gmail.com"));
        }


        List<Gruppe> gruppenSonnenschein = new ArrayList<>();
        Gruppe gruppeSonnenschein1 = new Gruppe("Hannah", "Ayse", 25, 24);
        gruppeRepository.save(gruppeSonnenschein1);
        Gruppe gruppeSonnenschein2 = new Gruppe("Elena", "Thomas", 25, 22);
        gruppeRepository.save(gruppeSonnenschein2);
        gruppenSonnenschein.add(gruppeSonnenschein1);
        gruppenSonnenschein.add(gruppeSonnenschein2);
        Adresse adresseSonnenschein = new Adresse("Tulbeckstrasse", "30", "80339", "München");
        adresseRepository.save(adresseSonnenschein);
        Person michael = new Person("Michael", "Leitung","Hallo, ich heiße Michael");
        personRepository.save(michael);

        List<Gruppe> gruppenMumula = new ArrayList<>();
        Gruppe gruppeMumula1 = new Gruppe("Marlene", "Lisa", 30, 29);
        gruppeRepository.save(gruppeMumula1);
        Gruppe gruppeMumula2 = new Gruppe("Jackson", "Isabell", 30, 30);
        gruppeRepository.save(gruppeMumula2);
        gruppenMumula.add(gruppeMumula1);
        gruppenMumula.add(gruppeMumula2);

        Adresse adresseMumula = new Adresse("Schlichtstrasse", "1", "85055", "ingolstadt");
        adresseRepository.save(adresseMumula);

        Person caner = new Person("Caner", "Leitung","Hallo ich heiße Caner");
        personRepository.save(caner);

        Kita kita1 = new Kita("Sonnenschein", adresseSonnenschein, gruppenSonnenschein, michael, 170);
        kitaRepository.save(kita1);
        Kita kita2 = new Kita("Mumula", adresseMumula, gruppenMumula, caner, 210);
        kitaRepository.save(kita2);


    }
}
