package com.example.server.user;

public class UserDTO {

    private String username;

    private String password;

//    private String userEmail;

    private boolean isAdmin;

    public UserDTO() {
    }

    public UserDTO(String username) {
        this.username = username;
        this.password = password;
//        this.userEmail = userEmail;
        this.isAdmin = isAdmin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public String getUserEmail() {
//        return userEmail;
//    }
//
//    public void setUserEmail(String userEmail) {
//        this.userEmail = userEmail;
//    }
}
