package com.example.server.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Entity
@Table(name = "USERS")
public class User {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type = "uuid-char")
    private UUID id;

    @NotEmpty
    private String username;
    private String password;
  //  private String userEmail;



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public String getUserEmail() {
//        return userEmail;
//    }

//    public void setUserEmail(String userEmail) {
//        this.userEmail = userEmail;
//    }

    public User(String userName, String password, String userEmail) {
        this.username = userName;
        this.password = password;
//        this.userEmail = userEmail;
    }

    public User(@NotEmpty String username) {
        this.username = this.username;
    }

    public User() {
    }


    @JsonIgnore
    public String getPassword() {
        return password;
    }
}
