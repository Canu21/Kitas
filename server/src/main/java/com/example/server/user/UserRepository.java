package com.example.server.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByUsername(String username);

    // User findByUsername(String username);
    Optional<User> findFirstByUsernameAndPassword(String username, String password);

    boolean existsByUsername(String username);

    Optional<User> findById(Long id);

   // boolean existsByEmail(String email);

}
