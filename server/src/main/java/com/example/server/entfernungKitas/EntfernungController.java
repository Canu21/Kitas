package com.example.server.entfernungKitas;

import com.example.server.kita.Kita;
import com.example.server.kita.KitaDTO;
import com.example.server.kita.KitaRepository;
import com.example.server.kita.KitaService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/kita")
public class EntfernungController {

    @Autowired
    private KitaRepository kitaRepository;

    @Autowired
    private KitaService kitaService;


    @PostMapping("/currentUserData")
    public void naheGelegenes(EntfernungDTO entfernungDTO) throws InterruptedException, ApiException, IOException {
        List<Kita> kitas = kitaService.getFreeKitas(kitaRepository);
        String adressData = entfernungDTO.getStrasse() + " " + entfernungDTO.getHausnummer() + " " + entfernungDTO.getPostleitzahl() + " " + entfernungDTO.getStadt();

        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyDq9xtuQKCwEZPSwMYR91fNbvdFVlgFHYw")
                .build();
        GeocodingResult[] userData = GeocodingApi.geocode(context,
                adressData).await();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
//        System.out.println(gson.toJson(userData[0]));
        GeometryModel aktullerGeometrymodel = new GeometryModel();
        aktullerGeometrymodel.setLat(gson.toJson(userData[0].geometry.location.lat));
        aktullerGeometrymodel.setLng(gson.toJson(userData[0].geometry.location.lng));
        aktullerGeometrymodel.setName("GeometrieUser");

        List<GeometryModel>kitasGeometryModel = new ArrayList<>();
        for (Kita kita : kitas) {
            String address = kita.getAdresse().getStrasse() + " "
                    + kita.getAdresse().getHausnummer() + " "
                    + kita.getAdresse().getPostleitzahl() + " "
                    + kita.getAdresse().getWohnort();

            GeocodingResult[] kitadata = GeocodingApi.geocode(context,
                    address).await();
            GeometryModel geometryModel = new GeometryModel();
            geometryModel.setName(kita.getName());
            geometryModel.setLng(gson.toJson(kitadata[0].geometry.location.lng));
            geometryModel.setLat(gson.toJson(kitadata[0].geometry.location.lat));
            kitasGeometryModel.add(geometryModel);
        }
        String naehstgelegeneKita = "";
        double temp = Double.MAX_VALUE;

        for (GeometryModel geometryModel : kitasGeometryModel){

           double aktuelleEntfernung = EntfernungZwischen2KoordinatenService.berechneEntfernung(Double.parseDouble(aktullerGeometrymodel.getLat()),
                    Double.parseDouble(aktullerGeometrymodel.getLng()),
                    Double.parseDouble(geometryModel.getLat()),
                    Double.parseDouble(geometryModel.getLng()));
            if(aktuelleEntfernung < temp) {
                naehstgelegeneKita = geometryModel.getName();
            }
        }
        Kita naechsteKita = kitaRepository.findByName(naehstgelegeneKita).get();
        System.out.println(naechsteKita + "\nist " + temp + " Meter entfernt");
    }
}
