package com.example.server.freiePlaetze;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Beruf {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String erzieher;
    private String kinderpleger;
    private String leitung;

    public Beruf(String erzieher, String kinderpleger, String leitung) {
        this.erzieher = erzieher;
        this.kinderpleger = kinderpleger;
        this.leitung = leitung;
    }

    public Beruf() {
    }

    public String getErzieher() {
        return erzieher;
    }

    public void setErzieher(String erzieher) {
        this.erzieher = erzieher;
    }

    public String getKinderpleger() {
        return kinderpleger;
    }

    public void setKinderpleger(String kinderpleger) {
        this.kinderpleger = kinderpleger;
    }

    public String getLeitung() {
        return leitung;
    }

    public void setLeitung(String leitung) {
        this.leitung = leitung;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
