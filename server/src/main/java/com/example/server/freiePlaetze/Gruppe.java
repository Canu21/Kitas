package com.example.server.freiePlaetze;

import javax.persistence.*;

@Entity
public class Gruppe {

    @Id
    @Column(name="GRUPPE_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long gruppeId;

    private String erzieher;
    private String kinderpfleger;
    private int anzahlMaxKinder;
    private int anzahlFreiePlaetze;
    private int belegt;

    public Gruppe() {
    }

    public Gruppe(String erzieher, String kinderpfleger, int anzahlMaxKinder, int belegt) {
        this.erzieher = erzieher;
        this.kinderpfleger = kinderpfleger;
        this.anzahlMaxKinder = anzahlMaxKinder;
        this.belegt = belegt;
        this.anzahlFreiePlaetze = anzahlMaxKinder - belegt;
    }


    public Long getGruppeId() {
        return gruppeId;
    }

    public void setGruppeId(Long gruppeId) {
        this.gruppeId = gruppeId;
    }

    public String getErzieher() {
        return erzieher;
    }

    public void setErzieher(String erzieher) {
        this.erzieher = erzieher;
    }

    public String getKinderpfleger() {
        return kinderpfleger;
    }

    public void setKinderpfleger(String kinderpfleger) {
        this.kinderpfleger = kinderpfleger;
    }

    public int getAnzahlMaxKinder() {
        return anzahlMaxKinder;
    }

    public void setAnzahlMaxKinder(int anzahlMaxKinder) {
        this.anzahlMaxKinder = anzahlMaxKinder;
    }

    public int getAnzahlFreiePlaetze() {
        return anzahlFreiePlaetze;
    }

    public void setAnzahlFreiePlaetze(int anzahlFreiePlaetze) {
        this.anzahlFreiePlaetze = anzahlFreiePlaetze;
    }
}
