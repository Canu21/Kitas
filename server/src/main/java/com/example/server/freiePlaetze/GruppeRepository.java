package com.example.server.freiePlaetze;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GruppeRepository extends JpaRepository<Gruppe, Long> {
}
