package com.example.server.freiePlaetze;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BerufRepository extends JpaRepository<Beruf, Long> {
}
