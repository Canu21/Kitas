package com.example.server.freiePlaetze;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "PERSON")
public class Person {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String vorName;
    private String nachName;


    @JsonIgnore
    @Lob
    private byte[] profilBild;

    @Lob
    private String profilText;

    public Person() {
    }

    public Person(String vorName, String nachName, byte[] profilBild, String profilText) {
        this.vorName = vorName;
        this.nachName = nachName;
        this.profilBild = profilBild;
        this.profilText = profilText;
    }

    public Person(String vorName, String nachName, String profilText) {
        this.vorName = vorName;
        this.nachName = nachName;
        this.profilText = profilText;
    }

    public String getVorName() {
        return vorName;
    }

    public void setVorName(String vorName) {
        this.vorName = vorName;
    }

    public String getNachName() {
        return nachName;
    }

    public void setNachName(String nachName) {
        this.nachName = nachName;
    }

    public byte[] getProfilBild() {
        return profilBild;
    }

    public void setProfilBild(byte[] profilBild) {
        this.profilBild = profilBild;
    }

    public String getProfilText() {
        return profilText;
    }

    public void setProfilText(String profilText) {
        this.profilText = profilText;
    }

    public Long getId() {
        return id;
    }
}
