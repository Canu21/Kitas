package com.example.server.signup;

import com.example.server.kita.Kita;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SignUpRepository extends JpaRepository<Kita, Long> {
}
