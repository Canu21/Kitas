package com.example.server.signup;

import com.example.server.user.User;
import com.example.server.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SignUpService {

    @Autowired
    UserRepository userRepository;

    public User signUp(User user) {
        return userRepository.save(user);
    }
}
