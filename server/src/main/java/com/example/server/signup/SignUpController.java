package com.example.server.signup;

import com.example.server.api.SecurityService;
import com.example.server.user.User;
import com.example.server.user.UserDTO;
import com.example.server.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class SignUpController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SecurityService securityService;

    @PostMapping("/signup")
    public User signUp(@RequestBody UserDTO accountDTO) {
        if (userRepository.existsByUsername(accountDTO.getUsername())) {
            return new User("Der Benutzername ist schon vergeben");


        }

        User user = new User();

        user.setUsername(accountDTO.getUsername());
        user.setPassword(passwordEncoder.encode(accountDTO.getPassword()));
       // user.setUserEmail(accountDTO.getUserEmail());

        return userRepository.save(user);

    }


}
