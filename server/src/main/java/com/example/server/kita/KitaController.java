package com.example.server.kita;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/kita")
public class KitaController {

    @Autowired
    private KitaRepository kitaRepository;

    @Autowired
    private KitaService kitaService;

    @GetMapping("/freeKitas")
    public List<KitaDTO> freeKitas() {
        List<KitaDTO> output = new ArrayList<>();
        List<Kita> kitas = kitaService.getFreeKitas(kitaRepository);
        for (Kita k : kitas) {
            KitaDTO kitaDTO = new KitaDTO(k.getId(), k.getName(), k.getAdresse(), k.getGruppen(), k.getKitaLeitung(), k.getPreis());
            output.add(kitaDTO);
        }
        return output;
    }

    @GetMapping("/getAll")
    public List<KitaDTO> getAll() {
        List<KitaDTO> output = new ArrayList<>();
        List<Kita> kitas = kitaRepository.findAll();
        for(Kita kita : kitas){
            output.add(new KitaDTO(kita.getId(),kita.getName(),kita.getAdresse(),
                    kita.getGruppen(),kita.getKitaLeitung(),kita.getPreis()));
        }
        return output;
    }


}
