package com.example.server.kita;

import com.example.server.freiePlaetze.Adresse;
import com.example.server.freiePlaetze.Gruppe;
import com.example.server.freiePlaetze.Person;

import java.util.ArrayList;
import java.util.List;

public class KitaDTO {

    private Long id;
    private String name;
    private Adresse adresse;
    private List<Gruppe> gruppen = new ArrayList<>();
    private Person kitaLeitung;
    private int preis;

    public KitaDTO(Long id, String name, Adresse adresse, List<Gruppe> gruppen, Person kitaLeitung, int preis) {
        this.id = id;
        this.name = name;
        this.adresse = adresse;
        this.gruppen = gruppen;
        this.kitaLeitung = kitaLeitung;
        this.preis = preis;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public List<Gruppe> getGruppen() {
        return gruppen;
    }

    public void setGruppen(List<Gruppe> gruppen) {
        this.gruppen = gruppen;
    }

    public Person getKitaLeitung() {
        return kitaLeitung;
    }

    public void setKitaLeitung(Person kitaLeitung) {
        this.kitaLeitung = kitaLeitung;
    }

    public int getPreis() {
        return preis;
    }

    public void setPreis(int preis) {
        this.preis = preis;
    }
}
