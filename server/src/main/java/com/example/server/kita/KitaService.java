package com.example.server.kita;

import com.example.server.freiePlaetze.Gruppe;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KitaService {

    public List<Kita> getFreeKitas(KitaRepository kitaRepository) {

        List<Kita> kitas = kitaRepository.findAll();
        List<Kita> kitaOutput = new ArrayList<>();

        for (Kita k : kitas) {
            boolean checkPlaetze = false;
            for (Gruppe g : k.getGruppen()) {
                if (g.getAnzahlFreiePlaetze() > 0) {
                    checkPlaetze = true;
                }
            }
            if (checkPlaetze) {
                kitaOutput.add(k);
            }
        }
        return kitaOutput;
    }

}
