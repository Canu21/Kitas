package com.example.server.kita;

import com.example.server.freiePlaetze.Adresse;
import com.example.server.freiePlaetze.Gruppe;
import com.example.server.freiePlaetze.Person;
import com.example.server.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "KITA")
public class Kita {

    @Id
    @Column(name = "KITA_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToOne
    @JoinColumn(name = "adresse_id")
    private Adresse adresse;


    @OneToMany
    @JoinTable (
            name = "KITA_GRUPPEN",
            joinColumns = {@JoinColumn (name = "KITA_ID",referencedColumnName = "KITA_ID")},
            inverseJoinColumns = {@JoinColumn (name = "GRUPPE_ID",referencedColumnName = "GRUPPE_ID")}
    )
    private List<Gruppe> gruppen = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "kitaLeitung_id")
    private Person kitaLeitung;

    private int preis;

    public Kita(String name, Adresse adresse, List<Gruppe> gruppen, Person kitaLeitung, int preis) {
        this.name = name;
        this.adresse = adresse;
        this.gruppen = gruppen;
        this.kitaLeitung = kitaLeitung;
        this.preis = preis;
    }

    public Kita() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public List<Gruppe> getGruppen() {
        return gruppen;
    }

    public void setGruppen(List<Gruppe> gruppen) {
        this.gruppen = gruppen;
    }

    public int getPreis() {
        return preis;
    }

    public void setPreis(int preis) {
        this.preis = preis;
    }

    public Long getId() {
        return id;
    }

    public Person getKitaLeitung() {
        return kitaLeitung;
    }

    public void setKitaLeitung(Person kitaLeitung) {
        this.kitaLeitung = kitaLeitung;
    }

    @Override
    public String toString() {
        return "Kita{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", adresse=" + adresse +
                ", gruppen=" + gruppen +
                ", kitaLeitung=" + kitaLeitung +
                ", preis=" + preis +
                '}';
    }
}
