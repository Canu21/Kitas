package com.example.server.kita;

import com.example.server.kita.Kita;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface KitaRepository extends JpaRepository<Kita, Long> {

    Optional<Kita> findByName(String name);

}
